#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:a6f01bc28a2fcc26ecf941f22ebe55993235ed62; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:134217728:5975aa433e09f13e784f38c19e48b4a0fd389aa3 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:a6f01bc28a2fcc26ecf941f22ebe55993235ed62 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
